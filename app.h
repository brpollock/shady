#ifndef _APP_H_
#define _APP_H_

void app_error_callback(int error, const char* description);
void app_key_callback(GLFWwindow* window, int key, int scancode, int action, int mods);

int app_args(int argc, char **argv);
const char *app_window_title(void);
void app_init(void);
void app_deinit(void);
void app_render(GLFWwindow* window, double time,
                int pwidth, int pheight,
                int swidth, int sheight);

#endif //_APP_H_
