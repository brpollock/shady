ifeq ($(OS),)
 OS:=$(shell uname -s)
endif

CFLAGS_Linux  = `pkg-config --cflags glfw3`
CFLAGS_Darwin = `pkg-config --cflags glew glfw3`

LIBS_Linux  = `pkg-config --static --libs glfw3 gl glew`
LIBS_Darwin = `pkg-config --libs glew glfw3` -framework Cocoa -framework OpenGL -framework IOKit -framework CoreVideo


shady: app.c app.h main.c Makefile
	gcc $(CFLAGS_$(OS)) main.c app.c -o shady $(LIBS_$(OS))
