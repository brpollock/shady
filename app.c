#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>



static const struct
{
    float x, y;
    float r, g, b;
} vertices[4] =
{
    { -1.0f, -1.0f, 1.f, 0.f, 0.f },
    {  1.0f, -1.0f, 0.f, 1.f, 0.f },
    { -1.0f,  1.0f, 0.f, 0.f, 1.f },
    {  1.0f,  1.0f, 0.f, 1.f, 1.f }
};

static const char* vertex_shader_text =
"uniform vec2 iResolution;\n"
"attribute vec2 vPos;\n"
"varying vec2 fragCoord;\n"
"void main()\n"
"{\n"
"    fragCoord.x = (vPos.x+1.0)*iResolution.x*0.5;\n"
"    fragCoord.y = (-vPos.y+1.0)*iResolution.y*0.5;\n"
"    gl_Position = vec4(vPos, 0.0, 1.0);\n"
"}\n";

static const char* fragment_shader_template =
"uniform vec4 iMouse;\n"
"uniform vec2 iResolution;\n"
"uniform float iTime;\n"
"varying vec2 fragCoord;\n"
"void main()\n"
"{\n"
"    if(fragCoord.x == 100.5)\n"
"        gl_FragColor = vec4(1.0, 1.0, 1.0, 1.0);\n"
"    else\n"
"        gl_FragColor = vec4(sin(iTime+fragCoord.x*0.01), cos(iTime+fragCoord.y*0.01), 1.0, 1.0);\n"
"    if(iMouse.z >= 0.0) {\n"
"        if(distance(fragCoord.xy, iMouse.zw)<distance(iMouse.xy, iMouse.zw))\n"
"           gl_FragColor *= 0.7;\n"
"    }\n"
"}\n";


static GLuint vertex_buffer, vertex_shader, fragment_shader, program;
static GLint ires_location, itim_location, imou_location;
static GLint vpos_location;
static GLchar *fragment_shader_buf = NULL;

static char *shaderfilename = NULL;

int ms_state = GLFW_RELEASE;
GLfloat ms_cur_x = 0.0, ms_cur_y = 0.0,
        ms_prs_x = 1.0, ms_prs_y = 1.0;

static void errors(void)
{
    for(;;) {
        GLenum e = glGetError();
        if(e == GL_NO_ERROR)
            break;
        printf("glGetError: %d\n",e);
    }
}


static void compileShader(GLuint shader)
{
    GLint compileStatus;
    glCompileShader(shader);
    glGetShaderiv(shader, GL_COMPILE_STATUS, &compileStatus);
    if(compileStatus == GL_FALSE) {
        GLsizei log_length = 0;
        GLchar message[1024];
        glGetShaderInfoLog(shader, 1024, &log_length, message);
        printf("log:\n%s\n", message);
    }
}

int load_fragment_shader(void)
{
    FILE *f = NULL;
    long fsize;

    printf("Loading %s\n",shaderfilename);
    f = fopen(shaderfilename,"r");
    if(!f) {
        fprintf(stderr,"Error reading '%s' - failed to open\n",shaderfilename);
        return 2;
    }
    
    fseek(f, 0, SEEK_END);
    fsize = ftell(f);
    fseek(f, 0, SEEK_SET);
    
    free(fragment_shader_buf);
    fragment_shader_buf = malloc(fsize+1); // FIXME+leak if early-out
    fread(fragment_shader_buf, fsize, 1, f); // FIXME
    fragment_shader_buf[fsize] = 0;
    fclose(f);

    return 0;
}


static void do_shaders(void)
{
    glShaderSource(vertex_shader, 1, &vertex_shader_text, NULL);
    compileShader(vertex_shader);

    glShaderSource(fragment_shader, 1, (void*)&fragment_shader_buf, NULL); //FIXME?
    compileShader(fragment_shader);

    glAttachShader(program, vertex_shader);
    glAttachShader(program, fragment_shader);
    glLinkProgram(program);

    ires_location = glGetUniformLocation(program, "iResolution");
    itim_location = glGetUniformLocation(program, "iTime");
    imou_location = glGetUniformLocation(program, "iMouse");
    vpos_location = glGetAttribLocation(program, "vPos");

    glEnableVertexAttribArray(vpos_location);
    glVertexAttribPointer(vpos_location, 2, GL_FLOAT, GL_FALSE,
                          sizeof(float) * 5, (void*) 0);
}


//-- app_

void app_error_callback(int error, const char* description)
{
    fprintf(stderr, "Error: %s\n", description);
}

void app_key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    if(action == GLFW_PRESS) {
        load_fragment_shader();
        do_shaders();
    }
}


int app_args(int argc, char **argv)
{
    // 3 args: create and load file
    // 2 args: load file
    // else:   error
    FILE *f = NULL;
    if(argc == 3 && !strcmp(argv[1],"-c"))
    {
        f = fopen(argv[2],"wx");
        if(!f) {
            fprintf(stderr,"Error creating '%s' (%d)%s\n",argv[2],
                errno,
                errno==EEXIST?" - file exists":"");
            return 2;
        }
        fputs(fragment_shader_template, f);
        fclose(f);
    }
    else if(argc != 2)
    {
        printf("Usage: shady [-c] <fragmentshader>\n"
               "- load and render the named fragment shader\n"
               "- option '-c' creates the named shader if it doesn't exist\n");
        return 1;
    }

    shaderfilename = argv[argc-1];
    return load_fragment_shader();
}

const char *app_window_title(void)
{
    return "Shady";
}


void app_init(void)
{
    printf("OpenGL GL_VERSION = %s\n",glGetString(GL_VERSION));
    printf("OpenGL GL_VENDOR = %s\n",glGetString(GL_VENDOR));
    glViewport(0, 0, 640, 480);

    glGenBuffers(1, &vertex_buffer);
    glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    //-----------------------------------------------------
    vertex_shader = glCreateShader(GL_VERTEX_SHADER);
    fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
    program = glCreateProgram();

    do_shaders();
   //-----------------------------------------------------
}

void app_deinit(void)
{
    free(fragment_shader_buf);
}

void app_render(GLFWwindow* window, double time,
                int pwidth, int pheight,
                int swidth, int sheight)
{
    int state = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT);

    if (state == GLFW_PRESS) {
        double xpos, ypos;
        glfwGetCursorPos(window, &xpos, &ypos);
        if(ms_state != GLFW_PRESS) {
            ms_prs_x = xpos*pwidth/swidth;
            ms_prs_y = ypos*pheight/sheight;
        }
        ms_cur_x = xpos*pwidth/swidth;
        ms_cur_y = ypos*pheight/sheight;
    }
    ms_state = state;

    glClear(GL_COLOR_BUFFER_BIT);

    glUseProgram(program);
    if(state == GLFW_PRESS)
        glUniform4f(imou_location, (GLfloat)ms_cur_x, (GLfloat)ms_cur_y,
                                   (GLfloat)ms_prs_x, (GLfloat)ms_prs_y);
    else
        glUniform4f(imou_location, (GLfloat)ms_cur_x, (GLfloat)ms_cur_y,
                                   (GLfloat)-ms_prs_x, (GLfloat)-ms_prs_y);
    glUniform2f(ires_location, (GLfloat)pwidth, (GLfloat)pheight);
    glUniform1f(itim_location, (GLfloat)time);


    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
}


