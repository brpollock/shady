#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <stdio.h>
#include "app.h"


int main(int argc, char **argv)
{
    GLFWwindow* window;
    int r;

    r = app_args(argc, argv);
    if(r)
        return r;

    /* Initialize the library */
    glewExperimental = 1;
    if (!glfwInit())
        return -1;

    glfwSetErrorCallback(app_error_callback);

    /* Create a windowed mode window and its OpenGL context */
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
    window = glfwCreateWindow(640, 480, app_window_title(), NULL, NULL);
    if (!window)
    {
        glfwTerminate();
        return -1;
    }
    glfwSetKeyCallback(window, app_key_callback);

    /* Make the window's context current */
    glfwMakeContextCurrent(window);
    glewExperimental = 1;
    if (glewInit() != GLEW_OK) {
        return -1;
    }
    app_init();

    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window))
    {
        int widthpix, heightpix;
        int widthscrn, heightscrn;
        glfwGetFramebufferSize(window, &widthpix, &heightpix);
        glfwGetWindowSize(window, &widthscrn, &heightscrn);
        glViewport(0, 0, widthpix, heightpix);

        /* Render here */
        app_render(window, glfwGetTime(), widthpix, heightpix, widthscrn, heightscrn);

        /* Swap front and back buffers */
        glfwSwapBuffers(window);

        /* Poll for and process events */
        glfwPollEvents();
    }

    app_deinit();
    glfwTerminate();
    return 0;
}


