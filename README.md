# Shady


## Dependencies

### Ubuntu

```
sudo apt install libglfw3-dev libglfw3 libglew-dev
```

### OSX

- XCode command line tools
- glfw
    ```
    brew install pkg-config
    brew install glfw
    brew install glew
    ```


## Building

```
make
```

## Workflow

Create two Terminal tabs.

- in one tab, create a shader with: `./shady -c myshader`
    - or, if you've already created one: `./shady myshader`
- in the other tab, edit the shader
- press any key when the shady window is focused to reload the shader
    - any compile errors will be shown in the first tab

## Notes

Quick glsl version guide: https://github.com/mattdesl/lwjgl-basics/wiki/GLSL-Versions

